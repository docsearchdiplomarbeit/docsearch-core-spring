package codes.probst.docsearch.packagez.facade;

import java.util.Collection;
import java.util.Iterator;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import codes.probst.docsearch.ApplicationConfiguration;
import codes.probst.docsearch.TestEntityManagerConfiguration;
import codes.probst.docsearch.clazz.facade.ClassData;
import codes.probst.docsearch.clazz.model.ClassModel;
import codes.probst.docsearch.packages.facade.PackageData;
import codes.probst.docsearch.packages.facade.PackageFacade;
import codes.probst.docsearch.packages.facade.PackagePopulationOption;
import codes.probst.docsearch.packages.model.PackageModel;
import codes.probst.docsearch.project.model.ProjectModel;
import codes.probst.docsearch.version.facade.VersionData;
import codes.probst.docsearch.version.model.VersionModel;
import codes.probst.framework.pagination.Pageable;
import codes.probst.framework.pagination.Sort;
import codes.probst.framework.pagination.SortDirection;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {
	ApplicationConfiguration.class,
	TestEntityManagerConfiguration.class
})
@DirtiesContext(classMode = ClassMode.AFTER_EACH_TEST_METHOD)
public class PackageFacadeTest {

	@Autowired
	private PackageFacade facade;
	
	@PersistenceContext
	private EntityManager entityManager;
	
	@Before
	@Transactional
	public void setup() {
		ProjectModel project1 = insertProject("junit-group", "junit");
		ProjectModel project2 = insertProject("junit-group", "junit2");
		ProjectModel project3 = insertProject("junit-group2", "junit");
		
		VersionModel version1 = insertVersion("1.0.0", project1);
		VersionModel version2 = insertVersion("1.1.0", project1);
		VersionModel version3 = insertVersion("1.0.1", project1);
		VersionModel version4 = insertVersion("1.0", project2);
		VersionModel version5 = insertVersion("1.1", project3);
		
		PackageModel package1 = importPackage("com.example", version1);
		PackageModel package2 = importPackage("com.example", version2);
		package2.setDocumentation("Lorem ipsum. Dolor est.");
		persist(package2);
		
		PackageModel package3 = importPackage("com.example", version3);
		PackageModel package4 = importPackage("com.example.impl", version3);
		PackageModel package5 = importPackage("com.junit", version4);
		PackageModel package6 = importPackage("com.junit", version5);
		
		importClass("Example", package1);
		importClass("Example", package2);
		importClass("Example", package3);
		importClass("ExampleImpl", package4);
		importClass("Test", package5);
		importClass("Test", package6);
	}
	
	@Test
	@Transactional
	public void testGetByVersionId() {
		Collection<PackageData> packages = facade.getByVersionId(1l, null);
		Assert.assertNotNull(packages);
		Assert.assertEquals(1, packages.size());
		PackageData packagez = packages.iterator().next();

		Assert.assertEquals(Long.valueOf(1l), packagez.getId());
		Assert.assertEquals("com.example", packagez.getName());
	}
	
	@Test
	@Transactional
	public void testGetByKey() {
		Optional<PackageData> optional = facade.getByKey(1l);
		Assert.assertTrue(optional.isPresent());
		PackageData packagez = optional.get();

		Assert.assertEquals(Long.valueOf(1l), packagez.getId());
		Assert.assertEquals("com.example", packagez.getName());
	}
	
	@Test
	@Transactional
	public void testGetByClassId() {
		{
			Optional<PackageData> optional = facade.getByClassId(1l);
			Assert.assertTrue(optional.isPresent());
			PackageData packagez = optional.get();

			Assert.assertEquals(Long.valueOf(1l), packagez.getId());
			Assert.assertEquals("com.example", packagez.getName());
		}
		
		{
			Optional<PackageData> optional = facade.getByClassId(1l, PackagePopulationOption.OPTION_URL);
			Assert.assertTrue(optional.isPresent());
			PackageData packagez = optional.get();

			Assert.assertEquals(Long.valueOf(1l), packagez.getId());
			Assert.assertEquals("com.example", packagez.getName());
			Assert.assertEquals("/projects/junit/0/1.1.0/1/com.example/1", packagez.getUrl());
		}
		
		{
			Optional<PackageData> optional = facade.getByClassId(1l, PackagePopulationOption.OPTION_VERSION);
			Assert.assertTrue(optional.isPresent());
			PackageData packagez = optional.get();

			Assert.assertEquals(Long.valueOf(1l), packagez.getId());
			Assert.assertEquals("com.example", packagez.getName());
			
			VersionData version = packagez.getVersion();
			Assert.assertNotNull(version);

			Assert.assertEquals(Long.valueOf(1l), version.getId());
			Assert.assertEquals("1.1.0", version.getCode());
			Assert.assertEquals("/projects/junit/0/1.1.0/1", version.getUrl());
		}
		
		{
			Optional<PackageData> optional = facade.getByClassId(1l, PackagePopulationOption.OPTION_DOCUMENTATION);
			Assert.assertTrue(optional.isPresent());
			PackageData packagez = optional.get();

			Assert.assertEquals(Long.valueOf(1l), packagez.getId());
			Assert.assertEquals("com.example", packagez.getName());
			Assert.assertEquals("Lorem ipsum. Dolor est.", packagez.getDocumentation());
		}
		
		{
			Optional<PackageData> optional = facade.getByClassId(1l, PackagePopulationOption.OPTION_SHORT_DOCUMENTATION);
			Assert.assertTrue(optional.isPresent());
			PackageData packagez = optional.get();

			Assert.assertEquals(Long.valueOf(1l), packagez.getId());
			Assert.assertEquals("com.example", packagez.getName());
			Assert.assertEquals("Lorem ipsum.", packagez.getDocumentation());
		}
		
		{
			Optional<PackageData> optional = facade.getByClassId(1l, PackagePopulationOption.OPTION_CLASSES);
			Assert.assertTrue(optional.isPresent());
			PackageData packagez = optional.get();

			Assert.assertEquals(Long.valueOf(1l), packagez.getId());
			Assert.assertEquals("com.example", packagez.getName());
			Collection<ClassData> classes = packagez.getClasses();
			Assert.assertNotNull(classes);
			Assert.assertEquals(1, classes.size());
			
			ClassData clazz = classes.iterator().next();
			Assert.assertEquals(Long.valueOf(1l), clazz.getId());
			Assert.assertEquals("Example", clazz.getName());
			Assert.assertEquals("/projects/junit/0/1.1.0/1/com.example/1/Example/1", clazz.getUrl());
		}
	}
	
	@Test
	@Transactional
	public void testGetAll() {
		Pageable pageable = new Pageable();
		pageable.setSort(new Sort[] { new Sort("name", SortDirection.ASCENDING), new Sort("id", SortDirection.ASCENDING) });
		
		Collection<PackageData> packages = facade.getAll(pageable);
		Assert.assertNotNull(packages);
		Assert.assertEquals(6, packages.size());
		
		Iterator<PackageData> it = packages.iterator();
		
		{
			PackageData packagez = it.next();
			Assert.assertEquals(Long.valueOf(0l), packagez.getId());
			Assert.assertEquals("com.example", packagez.getName());
		}

		{
			PackageData packagez = it.next();
			Assert.assertEquals(Long.valueOf(1l), packagez.getId());
			Assert.assertEquals("com.example", packagez.getName());
		}

		{
			PackageData packagez = it.next();
			Assert.assertEquals(Long.valueOf(2l), packagez.getId());
			Assert.assertEquals("com.example", packagez.getName());
		}

		{
			PackageData packagez = it.next();
			Assert.assertEquals(Long.valueOf(3l), packagez.getId());
			Assert.assertEquals("com.example.impl", packagez.getName());
		}

		{
			PackageData packagez = it.next();
			Assert.assertEquals(Long.valueOf(4l), packagez.getId());
			Assert.assertEquals("com.junit", packagez.getName());
		}

		{
			PackageData packagez = it.next();
			Assert.assertEquals(Long.valueOf(5l), packagez.getId());
			Assert.assertEquals("com.junit", packagez.getName());
		}
	}
	

	private ClassModel importClass(String name, PackageModel packagez) {
		ClassModel clazz = new ClassModel();
		clazz.setName(name);
		clazz.setPackageModel(packagez);
		persist(clazz);
		return clazz;
	}
	
	private PackageModel importPackage(String name, VersionModel version) {
		PackageModel packagez = new PackageModel();
		packagez.setName(name);
		packagez.setVersion(version);
		persist(packagez);
		return packagez;
	}
	
	private VersionModel insertVersion(String code, ProjectModel project) {
		VersionModel version = new VersionModel();
		version.setCode(code);
		version.setProject(project);
		persist(version);
		return version;
	}
	
	private ProjectModel insertProject(String groupId, String artifactId) {
		ProjectModel project = new ProjectModel();
		project.setGroupId(groupId);
		project.setArtifactId(artifactId);
		persist(project);
		return project;
	}
	
	private void persist(Object object) {
		entityManager.persist(object);
	}
}
