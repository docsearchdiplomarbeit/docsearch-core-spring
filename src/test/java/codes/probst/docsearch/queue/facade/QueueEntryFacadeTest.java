package codes.probst.docsearch.queue.facade;

import java.io.IOException;
import java.util.Collection;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import codes.probst.docsearch.ApplicationConfiguration;
import codes.probst.docsearch.TestEntityManagerConfiguration;
import codes.probst.docsearch.analyzer.projectinformation.ProjectType;
import codes.probst.docsearch.queue.dao.QueueEntryDao;
import codes.probst.docsearch.queue.model.QueueEntryModel;
import codes.probst.docsearch.test.util.IOUtil;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {
	ApplicationConfiguration.class,
	TestEntityManagerConfiguration.class
})
@DirtiesContext(classMode = ClassMode.AFTER_EACH_TEST_METHOD)
public class QueueEntryFacadeTest {

	@Autowired
	private QueueEntryFacade facade;
	
	@Autowired
	private QueueEntryDao dao;
	
	
	@Test(expected = IllegalArgumentException.class)
	@Transactional
	public void testImportOfNonExistingCompiled() {
		facade.importEntry(null, new byte[] {1});
	}

	@Test(expected = IllegalArgumentException.class)
	@Transactional
	public void testImportOfNonExistingSource() {
		facade.importEntry(new byte[] {1}, null);
	}

	@Test(expected = IllegalArgumentException.class)
	@Transactional
	public void testImportOfInvalidData() {
		facade.importEntry(new byte[] {1}, new byte[] {1});
	}
	
	@Test
	@Transactional
	public void testImportOfExampleProject() throws IOException {
		facade.importEntry(
			IOUtil.readAll(getClass().getResourceAsStream("/example1-0.0.1-SNAPSHOT.jar"), 1024),
			IOUtil.readAll(getClass().getResourceAsStream("/example1-0.0.1-SNAPSHOT-sources.jar"), 1024)
		);
		
		Collection<QueueEntryModel> entries = dao.findAll(null);
		
		Assert.assertNotNull(entries);
		Assert.assertEquals(1, entries.size());
		
		{
			QueueEntryModel entry = entries.iterator().next();
			Assert.assertEquals("example1", entry.getArtifactId());
			Assert.assertEquals("codes.probst", entry.getGroupId());
			Assert.assertEquals("0.0.1-SNAPSHOT", entry.getVersion());
			Assert.assertEquals(ProjectType.MAVEN, entry.getType());
			Assert.assertNotNull(entry.getSourceData());
			Assert.assertNotNull(entry.getCompiledData());
		}
	}
	
}
