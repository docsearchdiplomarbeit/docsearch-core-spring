package codes.probst.docsearch.project.facade;

import java.util.Collection;
import java.util.Iterator;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import codes.probst.docsearch.ApplicationConfiguration;
import codes.probst.docsearch.TestEntityManagerConfiguration;
import codes.probst.docsearch.clazz.model.ClassModel;
import codes.probst.docsearch.packages.model.PackageModel;
import codes.probst.docsearch.project.model.ProjectModel;
import codes.probst.docsearch.version.facade.VersionData;
import codes.probst.docsearch.version.model.VersionModel;
import codes.probst.framework.pagination.Pageable;
import codes.probst.framework.pagination.Sort;
import codes.probst.framework.pagination.SortDirection;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {
	ApplicationConfiguration.class,
	TestEntityManagerConfiguration.class
})
@DirtiesContext(classMode = ClassMode.AFTER_EACH_TEST_METHOD)
public class ProjectFacadeTest {
	@Autowired
	private ProjectFacade facade;
	
	@PersistenceContext
	private EntityManager entityManager;
	
	private Long projectId1;
	private Long projectId2;
	private Long projectId3;
	
	private Long versionId1;

	private Long classId1;
	
	@Before
	@Transactional
	public void setup() {
		ProjectModel project1 = insertProject("junit-group", "junit");
		ProjectModel project2 = insertProject("junit-group", "junit2");
		ProjectModel project3 = insertProject("junit-group2", "junit");
		
		VersionModel version1 = insertVersion("1.0.0", project1);
		VersionModel version2 = insertVersion("1.1.0", project1);
		VersionModel version3 = insertVersion("1.0.1", project1);
		VersionModel version4 = insertVersion("1.0", project2);
		VersionModel version5 = insertVersion("1.1", project3);
		
		PackageModel package1 = importPackage("com.example", version1);
		PackageModel package2 = importPackage("com.example", version2);
		PackageModel package3 = importPackage("com.example", version3);
		PackageModel package4 = importPackage("com.example.impl", version3);
		PackageModel package5 = importPackage("com.junit", version4);
		PackageModel package6 = importPackage("com.junit", version5);
		
		ClassModel class1 = importClass("Example", package1);
		importClass("Example", package2);
		importClass("Example", package3);
		importClass("ExampleImpl", package4);
		importClass("Test", package5);
		importClass("Test", package6);
		
		this.projectId1 = project1.getId();
		this.projectId2 = project2.getId();
		this.projectId3 = project3.getId();
		
		this.versionId1 = version1.getId();
		
		this.classId1 = class1.getId();
	}
	
	@Test
	@Transactional
	public void testGetAll() {
		Pageable pageable = new Pageable();
		pageable.setSort(new Sort[] { new Sort("groupId", SortDirection.ASCENDING), new Sort("artifactId", SortDirection.ASCENDING) });
		
		Collection<ProjectData> projects = facade.getAll(pageable);
		Assert.assertNotNull(projects);
		Assert.assertEquals(3, projects.size());
		
		Iterator<ProjectData> it = projects.iterator();
		
		{
			ProjectData project = it.next();
			Assert.assertEquals(versionId1, project.getId());
			Assert.assertEquals("junit-group", project.getGroupId());
			Assert.assertEquals("junit", project.getArtifactId());
			Assert.assertNull(project.getUrl());
			Assert.assertNull(project.getVersions());
		}
		
		{
			ProjectData project = it.next();
			Assert.assertEquals(projectId2, project.getId());
			Assert.assertEquals("junit-group", project.getGroupId());
			Assert.assertEquals("junit2", project.getArtifactId());
			Assert.assertNull(project.getUrl());
			Assert.assertNull(project.getVersions());
		}
		
		{
			ProjectData project = it.next();
			Assert.assertEquals(projectId3, project.getId());
			Assert.assertEquals("junit-group2", project.getGroupId());
			Assert.assertEquals("junit", project.getArtifactId());
			Assert.assertNull(project.getUrl());
			Assert.assertNull(project.getVersions());
		}
		
		
		pageable.setCurrentPage(0);
		pageable.setPageSize(2);
		projects = facade.getAll(pageable);
		Assert.assertNotNull(projects);
		Assert.assertEquals(2, projects.size());
		
		it = projects.iterator();
		
		{
			ProjectData project = it.next();
			Assert.assertEquals(versionId1, project.getId());
			Assert.assertEquals("junit-group", project.getGroupId());
			Assert.assertEquals("junit", project.getArtifactId());
			Assert.assertNull(project.getUrl());
			Assert.assertNull(project.getVersions());
		}
		
		{
			ProjectData project = it.next();
			Assert.assertEquals(projectId2, project.getId());
			Assert.assertEquals("junit-group", project.getGroupId());
			Assert.assertEquals("junit2", project.getArtifactId());
			Assert.assertNull(project.getUrl());
			Assert.assertNull(project.getVersions());
		}
	}
	
	@Test
	@Transactional
	public void testGetByVersionId() {
		Optional<ProjectData> optional = facade.getByVersionId(Long.valueOf(Long.MAX_VALUE));
		Assert.assertFalse(optional.isPresent());
	
		optional = facade.getByVersionId(versionId1);
		Assert.assertTrue(optional.isPresent());
		
		{
			ProjectData project = optional.get();
			Assert.assertEquals(projectId1, project.getId());
			Assert.assertEquals("junit-group", project.getGroupId());
			Assert.assertEquals("junit", project.getArtifactId());
			Assert.assertNull(project.getUrl());
			Assert.assertNull(project.getVersions());
		}
		
		optional = facade.getByVersionId(versionId1, ProjectPopulationOption.OPTION_URL);
		Assert.assertTrue(optional.isPresent());
		
		{
			ProjectData project = optional.get();
			Assert.assertEquals(projectId1, project.getId());
			Assert.assertEquals("junit-group", project.getGroupId());
			Assert.assertEquals("junit", project.getArtifactId());
			Assert.assertEquals("/projects/junit/0", project.getUrl());
			Assert.assertNull(project.getVersions());
		}

		optional = facade.getByVersionId(versionId1, ProjectPopulationOption.OPTION_VERSIONS);
		Assert.assertTrue(optional.isPresent());
		
		{
			ProjectData project = optional.get();
			Assert.assertEquals(projectId1, project.getId());
			Assert.assertEquals("junit-group", project.getGroupId());
			Assert.assertEquals("junit", project.getArtifactId());
			Assert.assertNull(project.getUrl());
			Assert.assertNotNull(project.getVersions());
			Collection<VersionData> versions = project.getVersions();
			Assert.assertEquals(3, versions.size());
			Iterator<VersionData> it = versions.iterator();
			
			{
				VersionData version = it.next();
				Assert.assertEquals("1.0.0", version.getCode());
			}
			
			{
				VersionData version = it.next();
				Assert.assertEquals("1.1.0", version.getCode());
			}
			
			{
				VersionData version = it.next();
				Assert.assertEquals("1.0.1", version.getCode());
			}
		}
	}
	
	@Test
	@Transactional
	public void testGetByClassId() {
		Optional<ProjectData> optional = facade.getByClassId(Long.valueOf(Long.MAX_VALUE));
		Assert.assertFalse(optional.isPresent());
	
		optional = facade.getByClassId(classId1);
		Assert.assertTrue(optional.isPresent());
		
		{
			ProjectData project = optional.get();
			Assert.assertEquals(projectId1, project.getId());
			Assert.assertEquals("junit-group", project.getGroupId());
			Assert.assertEquals("junit", project.getArtifactId());
			Assert.assertNull(project.getUrl());
			Assert.assertNull(project.getVersions());
		}
		
		optional = facade.getByClassId(classId1, ProjectPopulationOption.OPTION_URL);
		Assert.assertTrue(optional.isPresent());
		
		{
			ProjectData project = optional.get();
			Assert.assertEquals(projectId1, project.getId());
			Assert.assertEquals("junit-group", project.getGroupId());
			Assert.assertEquals("junit", project.getArtifactId());
			Assert.assertEquals("/projects/junit/0", project.getUrl());
			Assert.assertNull(project.getVersions());
		}

		optional = facade.getByClassId(classId1, ProjectPopulationOption.OPTION_VERSIONS);
		Assert.assertTrue(optional.isPresent());
		
		{
			ProjectData project = optional.get();
			Assert.assertEquals(projectId1, project.getId());
			Assert.assertEquals("junit-group", project.getGroupId());
			Assert.assertEquals("junit", project.getArtifactId());
			Assert.assertNull(project.getUrl());
			Assert.assertNotNull(project.getVersions());
			Collection<VersionData> versions = project.getVersions();
			Assert.assertEquals(3, versions.size());
			Iterator<VersionData> it = versions.iterator();
			
			{
				VersionData version = it.next();
				Assert.assertEquals("1.0.0", version.getCode());
			}
			
			{
				VersionData version = it.next();
				Assert.assertEquals("1.1.0", version.getCode());
			}
			
			{
				VersionData version = it.next();
				Assert.assertEquals("1.0.1", version.getCode());
			}
		}
	}
	
	private ClassModel importClass(String name, PackageModel packagez) {
		ClassModel clazz = new ClassModel();
		clazz.setName(name);
		clazz.setPackageModel(packagez);
		persist(clazz);
		return clazz;
	}
	
	private PackageModel importPackage(String name, VersionModel version) {
		PackageModel packagez = new PackageModel();
		packagez.setName(name);
		packagez.setVersion(version);
		persist(packagez);
		return packagez;
	}
	
	private VersionModel insertVersion(String code, ProjectModel project) {
		VersionModel version = new VersionModel();
		version.setCode(code);
		version.setProject(project);
		persist(version);
		return version;
	}
	
	private ProjectModel insertProject(String groupId, String artifactId) {
		ProjectModel project = new ProjectModel();
		project.setGroupId(groupId);
		project.setArtifactId(artifactId);
		persist(project);
		return project;
	}
	
	private void persist(Object object) {
		entityManager.persist(object);
	}
}
