package codes.probst.docsearch.version.facade;

import java.util.Collection;
import java.util.Iterator;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import codes.probst.docsearch.ApplicationConfiguration;
import codes.probst.docsearch.TestEntityManagerConfiguration;
import codes.probst.docsearch.clazz.model.ClassModel;
import codes.probst.docsearch.packages.facade.PackageData;
import codes.probst.docsearch.packages.model.PackageModel;
import codes.probst.docsearch.project.facade.ProjectData;
import codes.probst.docsearch.project.model.ProjectModel;
import codes.probst.docsearch.version.model.VersionModel;
import codes.probst.framework.pagination.Pageable;
import codes.probst.framework.pagination.Sort;
import codes.probst.framework.pagination.SortDirection;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {
	ApplicationConfiguration.class,
	TestEntityManagerConfiguration.class
})
@DirtiesContext(classMode = ClassMode.AFTER_EACH_TEST_METHOD)
public class VersionFacadeTest {
	@Autowired
	private VersionFacade versionFacade;
	
	@PersistenceContext
	private EntityManager entityManager;
	
	@Before
	@Transactional
	public void setup() {
		ProjectModel project1 = insertProject("junit-group", "junit");
		ProjectModel project2 = insertProject("junit-group", "junit2");
		ProjectModel project3 = insertProject("junit-group2", "junit");
		
		VersionModel version1 = insertVersion("1.0.0", project1);
		VersionModel version2 = insertVersion("1.1.0", project1);
		VersionModel version3 = insertVersion("1.0.1", project1);
		VersionModel version4 = insertVersion("1.0", project2);
		VersionModel version5 = insertVersion("1.1", project3);
		
		PackageModel package1 = importPackage("com.example", version1);
		PackageModel package2 = importPackage("com.example", version2);
		PackageModel package3 = importPackage("com.example", version3);
		PackageModel package4 = importPackage("com.example.impl", version3);
		PackageModel package5 = importPackage("com.junit", version4);
		PackageModel package6 = importPackage("com.junit", version5);
		
		importClass("Example", package1);
		importClass("Example", package2);
		importClass("Example", package3);
		importClass("ExampleImpl", package4);
		importClass("Test", package5);
		importClass("Test", package6);
	}
	
	@Test
	@Transactional
	public void testGetByProjectId() {
		Collection<VersionData> versions = versionFacade.getByProjectId(1l);
		Assert.assertNotNull(versions);
		Assert.assertEquals(1, versions.size());
		Iterator<VersionData> it = versions.iterator();
		{
			VersionData version = it.next();
			Assert.assertEquals(Long.valueOf(3l), version.getId());
			Assert.assertEquals("1.0", version.getCode());
		}
	
	}
	
	@Test
	@Transactional
	public void testGetByPackageId() {
		Optional<VersionData> optional = versionFacade.getByPackageId(3l);
		Assert.assertTrue(optional.isPresent());
	
		VersionData version = optional.get();
		Assert.assertEquals(Long.valueOf(2l), version.getId());
		Assert.assertEquals("1.0.1", version.getCode());
	}
	
	@Test
	@Transactional
	public void testGetByKey() {
		Optional<VersionData> optional = versionFacade.getByKey(1l);
		Assert.assertTrue(optional.isPresent());
	
		VersionData version = optional.get();
		Assert.assertEquals(Long.valueOf(1l), version.getId());
		Assert.assertEquals("1.1.0", version.getCode());
	}
	
	@Test
	@Transactional
	public void testGetById() {
		{
			Optional<VersionData> optional = versionFacade.getByClassId(Long.valueOf(1));
			Assert.assertTrue(optional.isPresent());
		
			VersionData version = optional.get();
			Assert.assertEquals(Long.valueOf(1l), version.getId());
			Assert.assertEquals("1.1.0", version.getCode());
		}
		
		{
			Optional<VersionData> optional = versionFacade.getByClassId(Long.valueOf(1), VersionPopulationOption.OPTION_URL);
			Assert.assertTrue(optional.isPresent());
		
			VersionData version = optional.get();
			Assert.assertEquals(Long.valueOf(1l), version.getId());
			Assert.assertEquals("1.1.0", version.getCode());
			Assert.assertEquals("/projects/junit/0/1.1.0/1", version.getUrl());
		}
		
		{
			Optional<VersionData> optional = versionFacade.getByClassId(Long.valueOf(1), VersionPopulationOption.OPTION_PROJECT);
			Assert.assertTrue(optional.isPresent());
		
			VersionData version = optional.get();
			Assert.assertEquals(Long.valueOf(1l), version.getId());
			Assert.assertEquals("1.1.0", version.getCode());
			
			ProjectData project = version.getProject();
			Assert.assertNotNull(project);
			Assert.assertEquals("junit", project.getArtifactId());
			Assert.assertEquals("junit-group", project.getGroupId());
			Assert.assertEquals("/projects/junit/0", project.getUrl());
		}
		
		{
			Optional<VersionData> optional = versionFacade.getByClassId(Long.valueOf(1), VersionPopulationOption.OPTION_PACKAGES);
			Assert.assertTrue(optional.isPresent());
		
			VersionData version = optional.get();
			Assert.assertEquals(Long.valueOf(1l), version.getId());
			Assert.assertEquals("1.1.0", version.getCode());
			
			Collection<PackageData> packages = version.getPackages();
			Assert.assertNotNull(packages);
			Assert.assertEquals(1, packages.size());
			
			Iterator<PackageData> it = packages.iterator();
			
			{
				PackageData packagez = it.next();
				Assert.assertEquals("com.example", packagez.getName());
				Assert.assertEquals("/projects/junit/0/1.1.0/1/com.example/1", packagez.getUrl());
				Assert.assertEquals(Long.valueOf(1), packagez.getId());
			}
		}
	}
	
	@Test
	@Transactional
	public void testGetAll() {
		Pageable pageable = new Pageable();
		pageable.setSort(new Sort[] { new Sort("code", SortDirection.ASCENDING) });
		
		Collection<VersionData> versions = versionFacade.getAll(pageable);
		
		Assert.assertNotNull(versions);
		Assert.assertEquals(5, versions.size());
		
		Iterator<VersionData> it = versions.iterator();
		
		{
			VersionData version  = it.next();
			Assert.assertEquals(Long.valueOf(3), version.getId());
			Assert.assertEquals("1.0", version.getCode());
		}
		
		{
			VersionData version  = it.next();
			Assert.assertEquals(Long.valueOf(0), version.getId());
			Assert.assertEquals("1.0.0", version.getCode());
		}
		
		{
			VersionData version  = it.next();
			Assert.assertEquals(Long.valueOf(2), version.getId());
			Assert.assertEquals("1.0.1", version.getCode());
		}
		
		{
			VersionData version  = it.next();
			Assert.assertEquals(Long.valueOf(4), version.getId());
			Assert.assertEquals("1.1", version.getCode());
		}
		
		{
			VersionData version  = it.next();
			Assert.assertEquals(Long.valueOf(1), version.getId());
			Assert.assertEquals("1.1.0", version.getCode());
		}
	}
	

	
	private ClassModel importClass(String name, PackageModel packagez) {
		ClassModel clazz = new ClassModel();
		clazz.setName(name);
		clazz.setPackageModel(packagez);
		persist(clazz);
		return clazz;
	}
	
	private PackageModel importPackage(String name, VersionModel version) {
		PackageModel packagez = new PackageModel();
		packagez.setName(name);
		packagez.setVersion(version);
		persist(packagez);
		return packagez;
	}
	
	private VersionModel insertVersion(String code, ProjectModel project) {
		VersionModel version = new VersionModel();
		version.setCode(code);
		version.setProject(project);
		persist(version);
		return version;
	}
	
	private ProjectModel insertProject(String groupId, String artifactId) {
		ProjectModel project = new ProjectModel();
		project.setGroupId(groupId);
		project.setArtifactId(artifactId);
		persist(project);
		return project;
	}
	
	private void persist(Object object) {
		entityManager.persist(object);
	}
}
