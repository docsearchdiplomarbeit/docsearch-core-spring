package codes.probst.docsearch.clazz.facade;

import java.nio.charset.Charset;
import java.util.Collection;
import java.util.Iterator;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import codes.probst.docsearch.ApplicationConfiguration;
import codes.probst.docsearch.TestEntityManagerConfiguration;
import codes.probst.docsearch.clazz.model.ClassModel;
import codes.probst.docsearch.clazz.model.ClassUsageModel;
import codes.probst.docsearch.method.facade.MethodData;
import codes.probst.docsearch.method.model.MethodModel;
import codes.probst.docsearch.packages.facade.PackageData;
import codes.probst.docsearch.packages.model.PackageModel;
import codes.probst.docsearch.project.facade.ProjectData;
import codes.probst.docsearch.project.model.ProjectModel;
import codes.probst.docsearch.version.facade.VersionData;
import codes.probst.docsearch.version.model.VersionModel;
import codes.probst.framework.pagination.Pageable;
import codes.probst.framework.pagination.Sort;
import codes.probst.framework.pagination.SortDirection;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {
	ApplicationConfiguration.class,
	TestEntityManagerConfiguration.class
})
@DirtiesContext(classMode = ClassMode.AFTER_EACH_TEST_METHOD)
public class ClassFacadeTest {

	@Autowired
	private ClassFacade facade;
	
	@PersistenceContext
	private EntityManager entityManager;
	
	@Before
	@Transactional
	public void setup() {
		ProjectModel project1 = insertProject("junit-group", "junit");
		ProjectModel project2 = insertProject("junit-group", "junit2");
		ProjectModel project3 = insertProject("junit-group2", "junit");
		
		VersionModel version1 = insertVersion("1.0.0", project1);
		VersionModel version2 = insertVersion("1.1.0", project1);
		VersionModel version3 = insertVersion("1.0.1", project1);
		VersionModel version4 = insertVersion("1.0", project2);
		VersionModel version5 = insertVersion("1.1", project3);
		
		PackageModel package1 = importPackage("com.example", version1);
		PackageModel package2 = importPackage("com.example", version2);
		package2.setDocumentation("Lorem ipsum. Dolor est.");
		persist(package2);
		
		PackageModel package3 = importPackage("com.example", version3);
		PackageModel package4 = importPackage("com.example.impl", version3);
		PackageModel package5 = importPackage("com.junit", version4);
		PackageModel package6 = importPackage("com.junit", version5);
		
		ClassModel clazz1 = importClass("Example", package1);
		clazz1.setDocumentation("Lorem ipsum. Dolor est.");
		clazz1.setSource("public class Example {\n}".getBytes(Charset.forName("utf-8")));
		persist(clazz1);
		importClass("Example", package2);
		importClass("Example", package3);
		importClass("ExampleImpl", package4);
		importClass("Test", package5);
		importClass("Test", package6);
		
		importMethod("test", "test()", clazz1, clazz1);
		importMethod("execute", "execute()", clazz1, clazz1);
	}

	@Test
	@Transactional
	public void testByPackageId() {
		Collection<ClassData> classes = facade.getByPackageId(1l, null);
		Assert.assertNotNull(classes);
		Assert.assertEquals(1, classes.size());

		ClassData clazz = classes.iterator().next();
		Assert.assertEquals(Long.valueOf(1l), clazz.getId());
		Assert.assertEquals("Example", clazz.getName());
	}
	
	@Test
	@Transactional
	public void testGetByKey() {
		{
			Optional<ClassData> optional = facade.getByKey(0l);
			Assert.assertTrue(optional.isPresent());
			ClassData clazz = optional.get();
			Assert.assertEquals(Long.valueOf(0l), clazz.getId());
			Assert.assertEquals("Example", clazz.getName());
		}

		{
			Optional<ClassData> optional = facade.getByKey(0l, ClassPopulationOption.OPTION_SHORT_DESCRIPTION);
			Assert.assertTrue(optional.isPresent());
			ClassData clazz = optional.get();
			Assert.assertEquals(Long.valueOf(0l), clazz.getId());
			Assert.assertEquals("Example", clazz.getName());
			Assert.assertEquals("Lorem ipsum.", clazz.getDescription());
		}

		{
			Optional<ClassData> optional = facade.getByKey(0l, ClassPopulationOption.OPTION_DESCRIPTION);
			Assert.assertTrue(optional.isPresent());
			ClassData clazz = optional.get();
			Assert.assertEquals(Long.valueOf(0l), clazz.getId());
			Assert.assertEquals("Example", clazz.getName());
			Assert.assertEquals("Lorem ipsum. Dolor est.", clazz.getDescription());
		}

		{
			Optional<ClassData> optional = facade.getByKey(0l, ClassPopulationOption.OPTION_SOURCE);
			Assert.assertTrue(optional.isPresent());
			ClassData clazz = optional.get();
			Assert.assertEquals(Long.valueOf(0l), clazz.getId());
			Assert.assertEquals("Example", clazz.getName());
			Assert.assertEquals("public class Example {\n}", clazz.getSource());
		}

		{
			Optional<ClassData> optional = facade.getByKey(0l, ClassPopulationOption.OPTION_PROJECT);
			Assert.assertTrue(optional.isPresent());
			ClassData clazz = optional.get();
			Assert.assertEquals(Long.valueOf(0l), clazz.getId());
			Assert.assertEquals("Example", clazz.getName());
			ProjectData project = clazz.getProject();
			Assert.assertNotNull(project);
			Assert.assertEquals(Long.valueOf(0l), project.getId());
			Assert.assertEquals("junit-group", project.getGroupId());
			Assert.assertEquals("junit", project.getArtifactId());
			Assert.assertEquals("/projects/junit/0", project.getUrl());
		}

		{
			Optional<ClassData> optional = facade.getByKey(0l, ClassPopulationOption.OPTION_VERSION);
			Assert.assertTrue(optional.isPresent());
			ClassData clazz = optional.get();
			Assert.assertEquals(Long.valueOf(0l), clazz.getId());
			Assert.assertEquals("Example", clazz.getName());
			VersionData version = clazz.getVersion();
			Assert.assertNotNull(version);
			Assert.assertEquals(Long.valueOf(0l), version.getId());
			Assert.assertEquals("1.0.0", version.getCode());
			Assert.assertEquals("/projects/junit/0/1.0.0/0", version.getUrl());
		}

		{
			Optional<ClassData> optional = facade.getByKey(0l, ClassPopulationOption.OPTION_PACKAGE);
			Assert.assertTrue(optional.isPresent());
			ClassData clazz = optional.get();
			Assert.assertEquals(Long.valueOf(0l), clazz.getId());
			Assert.assertEquals("Example", clazz.getName());
			PackageData packagez = clazz.getPackagez();
			Assert.assertNotNull(packagez);
			Assert.assertEquals(Long.valueOf(0l), packagez.getId());
			Assert.assertEquals("com.example", packagez.getName());
			Assert.assertEquals("/projects/junit/0/1.0.0/0/com.example/0", packagez.getUrl());
		}

		{
			Optional<ClassData> optional = facade.getByKey(0l, ClassPopulationOption.OPTION_METHODS);
			Assert.assertTrue(optional.isPresent());
			ClassData clazz = optional.get();
			Assert.assertEquals(Long.valueOf(0l), clazz.getId());
			Assert.assertEquals("Example", clazz.getName());
			Collection<MethodData> methods = clazz.getMethods();
			Assert.assertNotNull(methods);
			Assert.assertEquals(2, methods.size());
			Iterator<MethodData> it = methods.iterator();
			{
				MethodData method = it.next();
				Assert.assertEquals("execute", method.getName());
				Assert.assertEquals("Example execute()", method.getSignature());
			}
			
			{
				MethodData method = it.next();
				Assert.assertEquals("test", method.getName());
				Assert.assertEquals("Example test()", method.getSignature());
			}

		}
	}
	
	@Test
	@Transactional
	public void testGetAll() {
		Pageable pageable = new Pageable();
		pageable.setSort(new Sort[] { new Sort("name", SortDirection.ASCENDING), new Sort("id", SortDirection.ASCENDING) });
		Collection<ClassData> classes = facade.getAll(pageable);
		Assert.assertNotNull(classes);
		Assert.assertEquals(6, classes.size());
		Iterator<ClassData> it = classes.iterator();
		
		{
			ClassData clazz = it.next();
			Assert.assertEquals(Long.valueOf(0l), clazz.getId());
			Assert.assertEquals("Example", clazz.getName());
		}
		
		{
			ClassData clazz = it.next();
			Assert.assertEquals(Long.valueOf(1l), clazz.getId());
			Assert.assertEquals("Example", clazz.getName());
		}
		
		{
			ClassData clazz = it.next();
			Assert.assertEquals(Long.valueOf(2l), clazz.getId());
			Assert.assertEquals("Example", clazz.getName());
		}
		
		{
			ClassData clazz = it.next();
			Assert.assertEquals(Long.valueOf(3l), clazz.getId());
			Assert.assertEquals("ExampleImpl", clazz.getName());
		}
		
		{
			ClassData clazz = it.next();
			Assert.assertEquals(Long.valueOf(4l), clazz.getId());
			Assert.assertEquals("Test", clazz.getName());
		}
		
		{
			ClassData clazz = it.next();
			Assert.assertEquals(Long.valueOf(5l), clazz.getId());
			Assert.assertEquals("Test", clazz.getName());
		}
	}

	private MethodModel importMethod(String name, String signature, ClassModel returnType, ClassModel clazz) {
		ClassUsageModel returnTypeUsage = new ClassUsageModel();
		returnTypeUsage.setArray(false);
		returnTypeUsage.setClazz(returnType);
		persist(returnTypeUsage);
		
		MethodModel method = new MethodModel();
		method.setName(name);
		method.setSignature(signature);
		method.setReturnType(returnTypeUsage);
		method.setClazz(clazz);
		persist(method);
		return method;
	}

	private ClassModel importClass(String name, PackageModel packagez) {
		ClassModel clazz = new ClassModel();
		clazz.setName(name);
		clazz.setPackageModel(packagez);
		persist(clazz);
		return clazz;
	}
	
	private PackageModel importPackage(String name, VersionModel version) {
		PackageModel packagez = new PackageModel();
		packagez.setName(name);
		packagez.setVersion(version);
		persist(packagez);
		return packagez;
	}
	
	private VersionModel insertVersion(String code, ProjectModel project) {
		VersionModel version = new VersionModel();
		version.setCode(code);
		version.setProject(project);
		persist(version);
		return version;
	}
	
	private ProjectModel insertProject(String groupId, String artifactId) {
		ProjectModel project = new ProjectModel();
		project.setGroupId(groupId);
		project.setArtifactId(artifactId);
		persist(project);
		return project;
	}
	
	private void persist(Object object) {
		entityManager.persist(object);
	}
}
