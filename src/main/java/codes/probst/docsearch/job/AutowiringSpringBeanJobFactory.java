package codes.probst.docsearch.job;

import org.quartz.spi.TriggerFiredBundle;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.scheduling.quartz.SpringBeanJobFactory;

public class AutowiringSpringBeanJobFactory extends SpringBeanJobFactory implements ApplicationContextAware {
	private AutowireCapableBeanFactory factory;
	
	@Override
	protected Object createJobInstance(TriggerFiredBundle bundle) throws Exception {
		 Object job = super.createJobInstance(bundle);
		 factory.autowireBean(job);
		 return job;
	}

	@Override
	public void setApplicationContext(ApplicationContext ctx) throws BeansException {
		this.factory = ctx.getAutowireCapableBeanFactory();
	}
	
}
