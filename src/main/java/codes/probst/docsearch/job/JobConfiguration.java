package codes.probst.docsearch.job;


import java.io.IOException;
import java.util.Properties;

import javax.sql.DataSource;

import org.quartz.JobDetail;
import org.quartz.Trigger;
import org.quartz.spi.JobFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.PropertiesFactoryBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;

@Configuration
public class JobConfiguration {
	
	@Autowired
	private PlatformTransactionManager transactionManager;
	
	@Autowired
	private ApplicationContext context;
	
	@Autowired
	private DataSource dataSource;
	
	@Bean
	public SchedulerFactoryBean schedulerFactoryBean() {
		SchedulerFactoryBean quartzScheduler = new SchedulerFactoryBean();
		
		quartzScheduler.setTransactionManager(transactionManager);
		quartzScheduler.setQuartzProperties(quartzProperties());
		quartzScheduler.setOverwriteExistingJobs(true);
		quartzScheduler.setJobFactory(jobFactory());
		quartzScheduler.setTriggers(context.getBeansOfType(Trigger.class).values().stream().toArray(i -> new Trigger[i]));
		quartzScheduler.setJobDetails(context.getBeansOfType(JobDetail.class).values().stream().toArray(i -> new JobDetail[i]));
		quartzScheduler.setDataSource(dataSource);
		quartzScheduler.setNonTransactionalDataSource(dataSource);
		
		return quartzScheduler;
	}
	
	@Bean
	public JobFactory jobFactory() {
		AutowiringSpringBeanJobFactory factory = new AutowiringSpringBeanJobFactory();
		
		
		return factory;
	}
	
	@Bean
	public Properties quartzProperties() {
		PropertiesFactoryBean propertiesFactoryBean = new PropertiesFactoryBean();
		propertiesFactoryBean.setLocation(new ClassPathResource("/quartz.properties"));
		Properties properties = null;
		try {
			propertiesFactoryBean.afterPropertiesSet();
			properties = propertiesFactoryBean.getObject();

		} catch (IOException e) {
			throw new RuntimeException(e.getMessage(), e);
		}

		return properties;
	}
	
}
