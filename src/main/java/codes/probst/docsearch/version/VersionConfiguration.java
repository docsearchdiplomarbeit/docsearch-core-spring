package codes.probst.docsearch.version;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import codes.probst.docsearch.version.dao.JpaVersionDao;
import codes.probst.docsearch.version.dao.VersionDao;
import codes.probst.docsearch.version.facade.DefaultVersionFacade;
import codes.probst.docsearch.version.facade.SpringVersionFacade;
import codes.probst.docsearch.version.facade.VersionData;
import codes.probst.docsearch.version.facade.VersionFacade;
import codes.probst.docsearch.version.facade.VersionPopulationOption;
import codes.probst.docsearch.version.facade.converter.DefaultVersionConverter;
import codes.probst.docsearch.version.facade.populator.VersionPackagesPopulator;
import codes.probst.docsearch.version.facade.populator.VersionProjectPopulator;
import codes.probst.docsearch.version.facade.populator.VersionUrlPopulator;
import codes.probst.docsearch.version.model.VersionModel;
import codes.probst.docsearch.version.service.DefaultVersionService;
import codes.probst.docsearch.version.service.VersionService;
import codes.probst.docsearch.version.url.DefaultVersionUrlResolver;
import codes.probst.framework.bean.BeanResolver;
import codes.probst.framework.converter.Converter;
import codes.probst.framework.populator.ConfigurablePopulator;
import codes.probst.framework.populator.DefaultConfigurablePopulator;
import codes.probst.framework.populator.PopulationOption;
import codes.probst.framework.populator.Populator;
import codes.probst.framework.url.UrlCleaner;
import codes.probst.framework.url.UrlResolver;

@Configuration
public class VersionConfiguration {
	@PersistenceContext
	private EntityManager entityManager;
	
	@Autowired
	private UrlCleaner urlCleaner;
	
	@Autowired
	private BeanResolver beanResolver;
	
	@Bean
	public VersionDao versionDao() {
		JpaVersionDao dao = new JpaVersionDao();
		
		dao.setEntityManager(entityManager);
		
		return dao;
	}
	
	@Bean
	public VersionService versionService() {
		DefaultVersionService service = new DefaultVersionService();
		
		service.setDao(versionDao());
		
		return service;
	}
	
	@Bean
	public VersionFacade versionFacade() {
		DefaultVersionFacade facade = new SpringVersionFacade();
		
		facade.setConverter(versionConverter());
		facade.setPopulator(versionPopulator());
		facade.setService(versionService());
		
		return facade;
	}
	
	@Bean
	public Converter<VersionModel, VersionData> versionConverter() {
		DefaultVersionConverter converter = new DefaultVersionConverter();
		
		return converter;
	}
	
	@Bean
	public ConfigurablePopulator<VersionModel, VersionData> versionPopulator() {
		DefaultConfigurablePopulator<VersionModel, VersionData> populator = new DefaultConfigurablePopulator<>();
		
		populator.setPopulators(versionPopulators());
		
		return populator;
	}
	
	@Bean
	public Map<PopulationOption, Collection<Populator<VersionModel, VersionData>>> versionPopulators() {
		Map<PopulationOption, Collection<Populator<VersionModel, VersionData>>> map = new HashMap<>();
		
		map.put(VersionPopulationOption.OPTION_URL, Arrays.asList(
			versionUrlPopulator()
		));
		
		map.put(VersionPopulationOption.OPTION_PACKAGES, Arrays.asList(
			versionPackagesPopulator()
		));
		
		map.put(VersionPopulationOption.OPTION_PROJECT, Arrays.asList(
			versionProjectPopulator()
		));
		
		
		
		return map;
	}
	
	@Bean
	public Populator<VersionModel, VersionData> versionUrlPopulator() {
		VersionUrlPopulator populator = new VersionUrlPopulator();
		
		populator.setResolver(versionUrlResolver());
		
		return populator;
	}
	
	@Bean
	public UrlResolver<VersionModel> versionUrlResolver() {
		DefaultVersionUrlResolver resolver = new DefaultVersionUrlResolver();
		
		resolver.setUrlCleaner(urlCleaner);
		
		return resolver;
	}
	
	@Bean
	public Populator<VersionModel, VersionData> versionPackagesPopulator() {
		VersionPackagesPopulator populator = new VersionPackagesPopulator();
		
		populator.setBeanResolver(beanResolver);
		
		return populator;
	}
	
	@Bean
	public Populator<VersionModel, VersionData> versionProjectPopulator() {
		VersionProjectPopulator populator = new VersionProjectPopulator();
		
		populator.setBeanResolver(beanResolver);
		
		return populator;
	}
	
}
