package codes.probst.docsearch.version.facade;

import java.util.Collection;
import java.util.Optional;

import org.springframework.transaction.annotation.Transactional;

import codes.probst.framework.pagination.Pageable;
import codes.probst.framework.populator.PopulationOption;

public class SpringVersionFacade extends DefaultVersionFacade {

	@Transactional
	@Override
	public Collection<VersionData> getAll(Pageable pageable, PopulationOption... options) {
		return super.getAll(pageable, options);
	}

	@Transactional
	@Override
	public Optional<VersionData> getByKey(Long key, PopulationOption... options) {
		return super.getByKey(key, options);
	}

	@Transactional
	@Override
	public Optional<VersionData> getByClassId(Long classId, VersionPopulationOption... options) {
		return super.getByClassId(classId, options);
	}

	@Transactional
	@Override
	public Optional<VersionData> getByPackageId(Long packageId, VersionPopulationOption... options) {
		return super.getByPackageId(packageId, options);
	}
	
	@Transactional
	@Override
	public Collection<VersionData> getByProjectId(Long projectId, VersionPopulationOption... options) {
		// TODO Auto-generated method stub
		return super.getByProjectId(projectId, options);
	}
}
