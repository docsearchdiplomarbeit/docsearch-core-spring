package codes.probst.docsearch.search;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.text.ParseException;
import java.util.HashMap;
import java.util.Map;

import org.elasticsearch.client.Client;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.InetSocketTransportAddress;
import org.quartz.JobDetail;
import org.quartz.Trigger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.scheduling.quartz.CronTriggerFactoryBean;
import org.springframework.scheduling.quartz.JobDetailFactoryBean;

import codes.probst.docsearch.clazz.model.ClassModel;
import codes.probst.docsearch.clazz.service.ClassService;
import codes.probst.docsearch.packages.model.PackageModel;
import codes.probst.docsearch.packages.service.PackageService;
import codes.probst.docsearch.project.model.ProjectModel;
import codes.probst.docsearch.search.facade.DefaultSearchFacade;
import codes.probst.docsearch.search.facade.SearchFacade;
import codes.probst.docsearch.search.index.DefaultIndexStrategy;
import codes.probst.docsearch.search.index.IndexStrategy;
import codes.probst.docsearch.search.index.IndexType;
import codes.probst.docsearch.search.index.SpringIndexStrategy;
import codes.probst.docsearch.search.job.SpringSearchIndexJob;
import codes.probst.docsearch.search.service.DefaultSearchService;
import codes.probst.docsearch.search.service.SearchService;
import codes.probst.docsearch.version.model.VersionModel;
import codes.probst.docsearch.version.service.VersionService;
import codes.probst.framework.url.UrlResolver;

@Configuration
public class SearchConfiguration {
	@Autowired
	private Environment environment;
	
	@Autowired
	private ClassService classService;
	
	@Autowired
	private PackageService packageService;
	
	@Autowired
	private VersionService versionService;
	
	@Autowired
	private UrlResolver<ProjectModel> projectUrlResolver;
	
	@Autowired
	private UrlResolver<VersionModel> versionUrlResolver;
	
	@Autowired
	private UrlResolver<PackageModel> packageUrlResolver;
	
	@Autowired
	private UrlResolver<ClassModel> classUrlResolver;
	
	@Bean
	public IndexStrategy indexStrategy() {
		DefaultIndexStrategy strategy = new SpringIndexStrategy();
		
		strategy.setClassService(classService);
		strategy.setSearchService(searchService());
		strategy.setPackageService(packageService);
		strategy.setVersionService(versionService);
		
		return strategy;
	}
	
	@Bean
	public Client elasticsearchClient() {
		String clusterName = environment.getProperty("elasticsearch.cluster");
		String host = environment.getProperty("elasticsearch.host");
		int port = environment.getProperty("elasticsearch.port", Integer.class);
		
		try {
			Client client = TransportClient
					.builder()
					.settings(Settings.builder().put("cluster.name", clusterName).build())
					.build()
					.addTransportAddress(new InetSocketTransportAddress(InetAddress.getByName(host), port));
			
			return client;
		} catch (UnknownHostException e) {
			throw new RuntimeException(e.getMessage(), e);
		}
	}
	
	@Bean
	public SearchService searchService() {
		DefaultSearchService service = new DefaultSearchService();
		
		service.setClassUrlResolver(classUrlResolver);
		service.setClient(elasticsearchClient());
		service.setPackageUrlResolver(packageUrlResolver);
		service.setProjectUrlResolver(projectUrlResolver);
		service.setVersionUrlResolver(versionUrlResolver);
		
		return service;
	}
	
	@Bean
	public JobDetail searchFullIndexJob() {
		JobDetailFactoryBean factory = new JobDetailFactoryBean();
		factory.setJobClass(SpringSearchIndexJob.class);
		factory.setGroup("search");
		factory.setName("full-index");
		Map<String, Object> map = new HashMap<>(1);
		map.put("type", IndexType.FULL);
		factory.setJobDataAsMap(map);
		factory.setDurability(true);
		factory.afterPropertiesSet();
		return factory.getObject();
	}

	
	@Bean
	public JobDetail searchUpdateIndexJob() {
		JobDetailFactoryBean factory = new JobDetailFactoryBean();
		factory.setJobClass(SpringSearchIndexJob.class);
		factory.setGroup("search");
		factory.setName("update-index");
		Map<String, Object> map = new HashMap<>(1);
		map.put("type", IndexType.UPDATE);
		factory.setJobDataAsMap(map);
		factory.setDurability(true);
		factory.afterPropertiesSet();
		return factory.getObject();
	}
	
	@Bean
	public Trigger searchUpdateIndexJobTrigger() throws ParseException {
		CronTriggerFactoryBean trigger = new CronTriggerFactoryBean();
		
		trigger.setCronExpression("0 0/5 * * * ?");
		trigger.setJobDetail(searchUpdateIndexJob());
		trigger.setGroup("search");
		trigger.setName("update-trigger");
		trigger.afterPropertiesSet();
		
		return trigger.getObject();
	}

	
	@Bean
	public SearchFacade searchFacade() {
		DefaultSearchFacade facade = new DefaultSearchFacade();
		
		facade.setSearchService(searchService());
		
		return facade;
	}
}
