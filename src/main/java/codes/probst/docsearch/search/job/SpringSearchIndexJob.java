package codes.probst.docsearch.search.job;

import org.springframework.beans.factory.annotation.Autowired;

import codes.probst.docsearch.search.index.IndexStrategy;

public class SpringSearchIndexJob extends SearchIndexJob {
	
	@Autowired
	@Override
	public void setStrategy(IndexStrategy strategy) {
		super.setStrategy(strategy);
	}
}
