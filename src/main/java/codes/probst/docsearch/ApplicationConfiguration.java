package codes.probst.docsearch;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.context.annotation.Import;

import codes.probst.docsearch.analyzer.AnalyzerConfiguration;
import codes.probst.docsearch.clazz.ClassConfiguration;
import codes.probst.docsearch.dependency.DependencyConfiguration;
import codes.probst.docsearch.file.FileConfiguration;
import codes.probst.docsearch.method.MethodConfiguration;
import codes.probst.docsearch.packagez.PackageConfiguration;
import codes.probst.docsearch.project.ProjectConfiguration;
import codes.probst.docsearch.queue.QueueConfiguration;
import codes.probst.docsearch.search.SearchConfiguration;
import codes.probst.docsearch.url.UrlConfiguration;
import codes.probst.docsearch.version.VersionConfiguration;
import codes.probst.framework.bean.BeanResolver;

@Configuration
@Import(value = {
	PropertiesConfiguration.class,
	AnalyzerConfiguration.class,
	FileConfiguration.class,
	ProjectConfiguration.class,
	VersionConfiguration.class,
	PackageConfiguration.class,
	ClassConfiguration.class,
	MethodConfiguration.class,
	DependencyConfiguration.class,
	QueueConfiguration.class,
	EntityManagerConfiguration.class,
	UrlConfiguration.class,
	SearchConfiguration.class
})
public class ApplicationConfiguration {

	@Bean
	public BeanResolver beanResolver() {
		return new SpringBeanResolver();
	}
	
}
