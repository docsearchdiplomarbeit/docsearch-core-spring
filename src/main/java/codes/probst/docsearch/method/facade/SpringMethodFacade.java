package codes.probst.docsearch.method.facade;

import java.util.Collection;
import java.util.Optional;

import org.springframework.transaction.annotation.Transactional;

import codes.probst.framework.pagination.Pageable;
import codes.probst.framework.populator.PopulationOption;

public class SpringMethodFacade extends DefaultMethodFacade {

	@Transactional
	@Override
	public Collection<MethodData> getAll(Pageable pageable, PopulationOption... options) {
		return super.getAll(pageable, options);
	}

	@Transactional
	@Override
	public Optional<MethodData> getByKey(Long key, PopulationOption... options) {
		return super.getByKey(key, options);
	}

	@Transactional
	@Override
	public Collection<MethodData> getByClassId(Long classId, Pageable pageable, MethodPopulationOption... options) {
		return super.getByClassId(classId, pageable, options);
	}
	
	
}
