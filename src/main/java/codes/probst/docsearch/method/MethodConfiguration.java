package codes.probst.docsearch.method;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;

import codes.probst.docsearch.clazz.facade.ClassData;
import codes.probst.docsearch.clazz.model.ClassModel;
import codes.probst.docsearch.method.dao.JpaMethodDao;
import codes.probst.docsearch.method.dao.JpaMethodUsageDao;
import codes.probst.docsearch.method.dao.MethodDao;
import codes.probst.docsearch.method.dao.MethodUsageDao;
import codes.probst.docsearch.method.facade.DefaultMethodFacade;
import codes.probst.docsearch.method.facade.MethodData;
import codes.probst.docsearch.method.facade.MethodFacade;
import codes.probst.docsearch.method.facade.MethodPopulationOption;
import codes.probst.docsearch.method.facade.MethodReferenceData;
import codes.probst.docsearch.method.facade.SpringMethodFacade;
import codes.probst.docsearch.method.facade.converter.DefaultMethodConverter;
import codes.probst.docsearch.method.facade.converter.DefaultMethodUsageConverter;
import codes.probst.docsearch.method.facade.populator.MethodReferencesPopulator;
import codes.probst.docsearch.method.facade.populator.MethodTagsPopulator;
import codes.probst.docsearch.method.model.MethodModel;
import codes.probst.docsearch.method.model.MethodUsageModel;
import codes.probst.docsearch.method.service.DefaultMethodService;
import codes.probst.docsearch.method.service.MethodService;
import codes.probst.framework.converter.Converter;
import codes.probst.framework.populator.ConfigurablePopulator;
import codes.probst.framework.populator.DefaultConfigurablePopulator;
import codes.probst.framework.populator.PopulationOption;
import codes.probst.framework.populator.Populator;
import codes.probst.framework.url.UrlResolver;

public class MethodConfiguration {
	@PersistenceContext
	private EntityManager entityManager;
	
	@Autowired
	private Converter<ClassModel, ClassData> classConverter;
	
	@Autowired
	private UrlResolver<ClassModel> classUrlResolver;
	
	@Bean
	public MethodDao methodDao() {
		JpaMethodDao dao = new JpaMethodDao();
		
		dao.setEntityManager(entityManager);
		
		return dao;
	}
	
	@Bean
	public MethodUsageDao methodUsageDao() {
		JpaMethodUsageDao dao = new JpaMethodUsageDao();
		
		dao.setEntityManager(entityManager);
		
		return dao;
	}
	
	@Bean
	public MethodService methodService() {
		DefaultMethodService service = new DefaultMethodService();
		
		service.setDao(methodDao());
		service.setMethodUsageDao(methodUsageDao());
		
		return service;
	}
	
	@Bean
	public Converter<MethodModel, MethodData> methodConverter() {
		DefaultMethodConverter converter = new DefaultMethodConverter();
		
		return converter;
	}
	
	@Bean
	public MethodFacade methodFacade() {
		DefaultMethodFacade facade = new SpringMethodFacade();
		
		facade.setConverter(methodConverter());
		facade.setPopulator(methodPopulator());
		facade.setService(methodService());
		
		return facade;
	}
	
	@Bean
	public ConfigurablePopulator<MethodModel, MethodData> methodPopulator() {
		DefaultConfigurablePopulator<MethodModel, MethodData> populator = new DefaultConfigurablePopulator<>();
		
		populator.setPopulators(methodPopulators());
		
		return populator;
	}
	
	@Bean
	public Map<PopulationOption, Collection<Populator<MethodModel, MethodData>>> methodPopulators() {
		Map<PopulationOption, Collection<Populator<MethodModel, MethodData>>> map = new HashMap<>();
		
		map.put(MethodPopulationOption.OPTION_REFERENCES, Arrays.asList(
			methodReferencesPopulator()
		));

		map.put(MethodPopulationOption.OPTION_TAGS, Arrays.asList(
			methodTagsPopulator()
		));
		
		return map;
	}
	
	@Bean
	public Populator<MethodModel, MethodData> methodReferencesPopulator() {
		MethodReferencesPopulator populator = new MethodReferencesPopulator();
		
		populator.setConverter(methodUsagePopulator());
		populator.setClassConverter(classConverter);
		populator.setMethodService(methodService());
		populator.setClassUrlResolver(classUrlResolver);
		
		return populator;
	}
	
	@Bean
	public Populator<MethodModel, MethodData> methodTagsPopulator() {
		MethodTagsPopulator populator = new MethodTagsPopulator();
		
		return populator;
	}
	
	@Bean
	public Converter<MethodUsageModel, MethodReferenceData> methodUsagePopulator() {
		DefaultMethodUsageConverter converter = new DefaultMethodUsageConverter();
		
		return converter;
	}
	
}
