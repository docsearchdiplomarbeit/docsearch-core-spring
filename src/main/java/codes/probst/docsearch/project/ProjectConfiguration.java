package codes.probst.docsearch.project;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;

import codes.probst.docsearch.project.dao.JpaProjectDao;
import codes.probst.docsearch.project.dao.ProjectDao;
import codes.probst.docsearch.project.facade.DefaultProjectFacade;
import codes.probst.docsearch.project.facade.ProjectData;
import codes.probst.docsearch.project.facade.ProjectFacade;
import codes.probst.docsearch.project.facade.ProjectPopulationOption;
import codes.probst.docsearch.project.facade.SpringProjectFacade;
import codes.probst.docsearch.project.facade.converter.DefaultProjectConverter;
import codes.probst.docsearch.project.facade.populator.ProjectUrlPopulator;
import codes.probst.docsearch.project.facade.populator.ProjectVersionsPopulator;
import codes.probst.docsearch.project.model.ProjectModel;
import codes.probst.docsearch.project.service.DefaultProjectService;
import codes.probst.docsearch.project.service.ProjectService;
import codes.probst.docsearch.project.url.DefaultProjectUrlResolver;
import codes.probst.framework.bean.BeanResolver;
import codes.probst.framework.converter.Converter;
import codes.probst.framework.populator.ConfigurablePopulator;
import codes.probst.framework.populator.DefaultConfigurablePopulator;
import codes.probst.framework.populator.PopulationOption;
import codes.probst.framework.populator.Populator;
import codes.probst.framework.url.UrlCleaner;
import codes.probst.framework.url.UrlResolver;

public class ProjectConfiguration {
	@PersistenceContext
	private EntityManager entityManager;
	
	@Autowired
	private UrlCleaner urlCleaner;
	
	@Autowired
	private BeanResolver beanResolver;
	
	@Bean
	public ProjectDao projectDao() {
		JpaProjectDao dao = new JpaProjectDao();
		
		dao.setEntityManager(entityManager);
		
		return dao;
	}
	
	@Bean
	public ProjectService projectService() {
		DefaultProjectService service = new DefaultProjectService();
		
		service.setDao(projectDao());
		
		return service;
	}
	
	@Bean
	public ProjectFacade projectFacade() {
		DefaultProjectFacade facade = new SpringProjectFacade();
		
		facade.setConverter(projectConverter());
		facade.setPopulator(projectPopulator());
		facade.setService(projectService());
		
		return facade;
	}
	
	@Bean
	public ConfigurablePopulator<ProjectModel, ProjectData> projectPopulator() {
		DefaultConfigurablePopulator<ProjectModel, ProjectData> populator = new DefaultConfigurablePopulator<>();
		
		populator.setPopulators(projectPopulators());
		
		return populator;
	}
	
	@Bean
	public Map<PopulationOption, Collection<Populator<ProjectModel, ProjectData>>> projectPopulators() {
		Map<PopulationOption, Collection<Populator<ProjectModel, ProjectData>>> map = new HashMap<>();
		
		map.put(ProjectPopulationOption.OPTION_URL, Arrays.asList(
			projectUrlPopulator()
		));

		map.put(ProjectPopulationOption.OPTION_VERSIONS, Arrays.asList(
			projectVersionsPopulator()
		));
		
		return map;
	}
	
	@Bean
	public Populator<ProjectModel, ProjectData> projectUrlPopulator() {
		ProjectUrlPopulator populator = new ProjectUrlPopulator();
		
		populator.setResolver(proejctUrlResolver());
		
		return populator;
	}
	
	@Bean
	public UrlResolver<ProjectModel> proejctUrlResolver() {
		DefaultProjectUrlResolver resolver = new DefaultProjectUrlResolver();
		
		resolver.setUrlCleaner(urlCleaner);
		
		return resolver;
	}
	
	
	@Bean
	public Converter<ProjectModel, ProjectData> projectConverter() {
		DefaultProjectConverter converter = new DefaultProjectConverter();
		
		return converter;
	}
	
	@Bean
	public Populator<ProjectModel, ProjectData> projectVersionsPopulator() {
		ProjectVersionsPopulator populator = new ProjectVersionsPopulator();
		
		populator.setBeanResolver(beanResolver);
		
		return populator;
	}
}
