package codes.probst.docsearch.project.facade;

import java.util.Collection;
import java.util.Optional;

import org.springframework.transaction.annotation.Transactional;

import codes.probst.framework.pagination.Pageable;
import codes.probst.framework.populator.PopulationOption;

public class SpringProjectFacade extends DefaultProjectFacade {

	@Transactional
	@Override
	public Collection<ProjectData> getAll(Pageable pageable, PopulationOption... options) {
		return super.getAll(pageable, options);
	}

	@Transactional
	@Override
	public Optional<ProjectData> getByKey(Long key, PopulationOption... options) {
		return super.getByKey(key, options);
	}

	@Transactional
	@Override
	public Optional<ProjectData> getByClassId(Long classId, ProjectPopulationOption... options) {
		return super.getByClassId(classId, options);
	}

	@Transactional
	@Override
	public Optional<ProjectData> getByVersionId(Long versionId, ProjectPopulationOption... options) {
		return super.getByVersionId(versionId, options);
	}
	
}
