package codes.probst.docsearch.clazz.facade;

import java.util.Collection;
import java.util.Optional;

import org.springframework.transaction.annotation.Transactional;

import codes.probst.framework.pagination.Pageable;
import codes.probst.framework.populator.PopulationOption;

public class SpringClassFacade extends DefaultClassFacade {

	@Transactional
	@Override
	public Collection<ClassData> getAll(Pageable pageable, PopulationOption... options) {
		return super.getAll(pageable, options);
	}

	@Transactional
	@Override
	public Optional<ClassData> getByKey(Long key, PopulationOption... options) {
		return super.getByKey(key, options);
	}

	@Transactional
	@Override
	public Collection<ClassData> getByPackageId(Long packageId, Pageable pageable, PopulationOption... options) {
		return super.getByPackageId(packageId, pageable, options);
	}

}
