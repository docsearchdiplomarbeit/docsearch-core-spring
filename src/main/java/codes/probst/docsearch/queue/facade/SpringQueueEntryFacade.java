package codes.probst.docsearch.queue.facade;

import org.springframework.transaction.annotation.Transactional;

public class SpringQueueEntryFacade extends DefaultQueueEntryFacade {

	@Transactional
	@Override
	public void importEntry(byte[] compiled, byte[] source) {
		super.importEntry(compiled, source);
	}
}
