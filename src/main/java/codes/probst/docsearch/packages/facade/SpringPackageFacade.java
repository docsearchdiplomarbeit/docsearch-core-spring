package codes.probst.docsearch.packages.facade;

import java.util.Collection;
import java.util.Optional;

import org.springframework.transaction.annotation.Transactional;

import codes.probst.framework.pagination.Pageable;
import codes.probst.framework.populator.PopulationOption;

public class SpringPackageFacade extends DefaultPackageFacade {

	@Transactional
	@Override
	public Collection<PackageData> getAll(Pageable pageable, PopulationOption... options) {
		return super.getAll(pageable, options);
	}

	@Transactional
	@Override
	public Optional<PackageData> getByKey(Long key, PopulationOption... options) {
		return super.getByKey(key, options);
	}

	@Transactional
	@Override
	public Optional<PackageData> getByClassId(Long classId, PackagePopulationOption... options) {
		return super.getByClassId(classId, options);
	}

	@Transactional
	@Override
	public Collection<PackageData> getByVersionId(Long versionId, Pageable pageable, PackagePopulationOption... options) {
		return super.getByVersionId(versionId, pageable, options);
	}
	
}
